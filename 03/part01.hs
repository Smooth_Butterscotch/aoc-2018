import Data.List
import qualified Data.Set as S

main = do
    input <- getInput
    print $ length $ getOverlap $ lines input

getInput :: IO String
getInput = readFile "input.txt"

type Rect = S.Set (Int, Int)

parseClaim :: String -> Rect
parseClaim s = let
    [_, _, sCoord, sSize] = words s
    (sX, _:sY) = break (==',') sCoord
    x = read sX
    y = read $ init sY
    (sWidth, _:sHeight) = break (=='x') sSize
    w = read sWidth
    h = read sHeight
    in S.fromList [(a, b) | a <- [x..x+w-1], b <- [y..y+h-1]]

getOverlap :: [String] -> Rect
getOverlap xs = let
    claims = map parseClaim xs
    (_, overlaps) = foldl go (mempty, mempty) claims
    in overlaps
    where
        go :: (Rect, Rect) -> Rect -> (Rect, Rect)
        go (full, dupes) x = (
            S.union x full,
            S.union (S.intersection x full) dupes
            )

