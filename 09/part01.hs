import Data.List

main = do
    input <- getInput
    print $ maximum $ players $ playGame $ parseInput input

getInput :: IO String
getInput = init <$> readFile "input.txt"

type Players = [Int]
type Circle = [Int]
type Marbles = [Int]
data Game = Game {
    players::Players,
    circle::Circle,
    marbles::Marbles
    } deriving (Show)

moveRight :: Int -> Circle -> Circle
moveRight n xs = let
    adj = n `rem` length xs
    in drop adj xs ++ take adj xs

moveLeft :: Int -> Circle -> Circle
moveLeft n xs = let
    adj = n `rem` length xs
    rot = length xs - adj
    in moveRight rot xs

takeTurn :: Game -> Game
takeTurn (Game all@(p:ps) cs (m:ms))
    | m `rem` 23 == 0 = let
        (x:xs) = moveLeft 7 cs
        s = p + m + x
        in Game (moveRight 1 (s:ps)) xs ms
    | otherwise = Game (moveRight 1 all) (m : moveRight 2 cs) (ms)

numPlayers :: String -> Int
numPlayers = read . head . words

numMarbles :: String -> Int
numMarbles = read . last . init . words

newGame :: Int -> Int -> Game
newGame ps ms = Game (replicate ps 0) ([0]) ([1..ms])

parseInput :: String -> Game
parseInput s = newGame (numPlayers s) (numMarbles s)

playGame :: Game -> Game
playGame = head . dropWhile hasMarbles . iterate takeTurn
    where
        hasMarbles :: Game -> Bool
        hasMarbles (Game _ _ []) = False
        hasMarbles _ = True

