import Data.List
import qualified Data.Set as S

main = do
    input <- getInput
    let (num, _) = getIsolated $ lines input
    putStrLn num

getInput :: IO String
getInput = readFile "input.txt"

type Rect = (String, S.Set (Int, Int))

parseClaim :: String -> Rect
parseClaim s = let
    ['#':num, _, sCoord, sSize] = words s
    (sX, _:sY) = break (==',') sCoord
    x = read sX
    y = read $ init sY
    (sWidth, _:sHeight) = break (=='x') sSize
    w = read sWidth
    h = read sHeight
    in (num, S.fromList [(a, b) | a <- [x..x+w-1], b <- [y..y+h-1]])

isIsolated :: Rect -> [Rect] -> Bool
isIsolated (n, r) = all (\(_, x) -> S.intersection x r == mempty)

getIsolated :: [String] -> Rect
getIsolated xs = let
    claims = map parseClaim xs
    parts = zipWith (,) <$> inits <*> tails $ claims
    in onlyIsolated $ init parts
    where
        onlyIsolated :: [([Rect], [Rect])] -> Rect
        onlyIsolated ((is, x:ts):rs)
            | isIsolated x (is++ts) = x
            | otherwise = onlyIsolated rs

