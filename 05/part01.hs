import Data.Char

main = do
    input <- getInput
    print $ length $ react input

getInput :: IO String
getInput = init <$> readFile "input.txt"

canReact :: Char -> Char -> Bool
canReact a b = abs (ord a - ord b) == 32

merge :: String -> String -> String
merge a b
    | a == "" = b
    | b == "" = a
    | canReact x y = merge xs ys
    | otherwise = a ++ b
    where
        (x, xs) = (last a, init a)
        (y, ys) = (head b, tail b)

react :: String -> String
react "" = ""
react [c] = [c]
react s = let
    (a, b) = splitAt (length s `div` 2) s
    in merge (react a) (react b)

