import Data.IntSet (IntSet, fromList, insert, member)

main = do
    input <- getInput
    print . finalFrequency $ input

getInput :: IO String
getInput = readFile "input.txt"

repeatList :: [a] -> [a]
repeatList = concat . repeat

getFrequencies :: String -> [String]
getFrequencies = repeatList . words

parseFrequency :: String -> Int
parseFrequency ('+':s) = read s
parseFrequency s = read s

dupeFrequency :: [Int] -> Int -> IntSet-> Int
dupeFrequency (f:fs) start seen
    | nextFreq `member` seen = nextFreq
    | otherwise = dupeFrequency fs nextFreq (insert nextFreq seen)
    where
        nextFreq = start + f

finalFrequency :: String -> Int
finalFrequency fs = dupeFrequency freqs 0 (fromList [0])
    where
        freqs = map parseFrequency . getFrequencies $ fs

