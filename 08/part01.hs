import Control.Monad.State

main = do
    input <- getInput
    print $ addAllMeta $ fst $ runState (parseTree) $ getNums input

getInput :: IO String
getInput = readFile "input.txt"

data Tree = Tree { children::[Tree], metadata::[Int] }

instance Show Tree where
    show Tree {children = c, metadata = m} = show c ++ show m

getNums :: String -> [Int]
getNums = map read . words

parseTrees :: Int -> State [Int] [Tree]
parseTrees 0 = return []
parseTrees n = do
    child <- parseTree
    children <- parseTrees (n - 1)
    return (child:children)

parseTree :: State [Int] Tree
parseTree = do
    (c:m:nums) <- get
    put nums
    children <- parseTrees c
    newNums <- get
    put $ drop m newNums
    return $ Tree children (take m newNums)

addAllMeta :: Tree -> Int
addAllMeta (Tree c m) = (sum m) + (sum $ map addAllMeta c)

