-- import qualified Data.Set as S
import qualified Data.Map as M
import Data.Maybe (fromJust)

type Serial = Int
type Point = (Int, Int)
type Size = Int
type Power = Int
type Square = ([Point], Size)
type Grid = M.Map Point Power
type PowerSquare = (Power, Point, Size)

main = do
    input <- getInput
    let size = 300
        g = getGrid (read input) size
        powers = map (getPowerSquare g) $ getAllSquares size
        p = maximum powers
    putStrLn $ showPowerSquare p

getInput :: IO String
getInput = readFile "input.txt"

getPower :: Serial -> Point -> Power
getPower s (x, y) = let
    rack = x + 10
    power = (rack * y + s) * rack
    dig = power `rem` 1000 `div` 100
    in dig - 5

getGrid :: Serial -> Size -> Grid
getGrid s size = let
    l = [let c = (a, b) in (c, getPower s c) | a <- [1..size], b <- [1..size]]
    in M.fromList l

getSquare :: Size -> Point -> Square
getSquare n (x, y) = ([(a, b) | a <- [x..x+n-1], b <- [y..y+n-1]], n)

getSquarePower :: Grid -> Square -> Power
getSquarePower cs = sum . map (fromJust . (flip M.lookup) cs) . fst

getAllSquares :: Size -> [Square]
getAllSquares size = let
    l = [
            let points = [(x, y) | x <- [1..size-n+1], y <- [1..size-n+1]]
            in map (getSquare n) points | n <- [1..size]
        ]
    in concat l

getPowerSquare :: Grid -> Square -> PowerSquare
getPowerSquare g s@(ps, n) = (getSquarePower g s, head $ fst s, n)

showPowerSquare :: PowerSquare -> String
showPowerSquare (_, (x, y), s) = show x ++ "," ++ show y ++ "," ++ show s

