import Data.Map (Map, keys, update, insert, empty, toList)

main = do
    input <- getInput
    print $ allCommon . lines $ input

getInput :: IO String
getInput = readFile "input.txt"

nearlyEqual :: String -> String -> Bool
nearlyEqual a b = [False] == (filter (not . id) $ zipWith (==) a b)

allNearlyEqual :: [String] -> [String]
allNearlyEqual xs = [x | x <- xs, y <- xs, x `nearlyEqual` y]

commonLetters :: String -> String -> String
commonLetters "" "" = ""
commonLetters (a:as) (b:bs)
    | a == b = a : commonLetters as bs
    | otherwise = commonLetters as bs

allCommon :: [String] -> String
allCommon xs = let [a, b] = allNearlyEqual xs in commonLetters a b

