import Data.Map (Map, keys, update, insert, empty, toList)

main = do
    input <- getInput
    print $ checksum . lines $ input

getInput :: IO String
getInput = readFile "input.txt"

occurences :: (Ord a) => [a] -> Map a Int
occurences = foldl (\acc x -> if x `elem` keys acc then update (\x -> Just $ x + 1) x acc else insert x 1 acc) empty

hasOccurences :: (Ord a) => Int -> [a] -> Bool
hasOccurences n xs = n `elem` (map snd . toList . occurences $ xs)

numHasOccurences :: (Ord a) => Int -> [[a]] -> Int
numHasOccurences n = length . filter id . map (hasOccurences n)

checksum :: [String] -> Int
checksum = (*) <$> numHasOccurences 2 <*> numHasOccurences 3

