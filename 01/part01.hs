main = do
    input <- getInput
    print . finalFrequency $ input

getInput :: IO String
getInput = readFile "input.txt"

getFrequencies :: String -> [String]
getFrequencies = words

parseFrequency :: String -> Int
parseFrequency ('+':s) = read s
parseFrequency s = read s

finalFrequency :: String -> Int
finalFrequency fs = sum . map parseFrequency . getFrequencies $ fs

