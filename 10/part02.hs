import qualified Data.Set as S

main = do
    input <- getInput
    let ps = getPoints $ lines input
    print $ snd $ moves ps

getInput :: IO String
getInput = init <$> readFile "input.txt"

type Pos = (Int, Int)
type Vel = (Int, Int)
type Point = (Pos, Vel)

parsePoint :: String -> Point
parsePoint s = let
    (_:beforeX) = dropWhile (/='<') s
    (sX, _:afterX) = break (==',') beforeX
    x = read sX
    (sY, _:afterY) = break (=='>') afterX
    y = read sY
    (_:beforeVx) = dropWhile (/='<') afterY
    (sVx, _:afterVx) = break (==',') beforeVx
    vx = read sVx
    (sVy, _) = break (=='>') afterVx
    vy = read sVy
    in ((x, y), (vx, vy))

getPoints :: [String] -> S.Set Point
getPoints = S.fromList . map parsePoint

movePoint :: Point -> Point
movePoint ((x, y), vel@(vx, vy)) = ((x + vx, y + vy), vel)

movePoints :: S.Set Point -> S.Set Point
movePoints = S.map movePoint

bounds :: S.Set Point -> (Pos, Pos)
bounds s = let
    left = S.findMin $ S.map (fst . fst) s
    right = S.findMax $ S.map (fst . fst) s
    top = S.findMin $ S.map (snd . fst) s
    bottom = S.findMax $ S.map (snd . fst) s
    in ((left, top), (right, bottom))

width :: S.Set Point -> Int
width s = let
    ((left, _), (right, _)) = bounds s
    in right - left + 1

height :: S.Set Point -> Int
height s = let
    ((_, top), (_, bottom)) = bounds s
    in bottom - top + 1

adjust :: S.Set Point -> S.Set Point
adjust s = let
    ((left, top), _) = bounds s
    in S.map (\((x, y), vel) -> ((x - left, y - top), vel)) s

chunks :: Int -> [a] -> [[a]]
chunks n xs
    | length xs <= n = [xs]
    | otherwise = take n xs : chunks n (drop n xs)

draw :: Pos -> S.Set Point -> Char
draw p s = if (S.filter ((==p) . fst) s) /= mempty then '#' else '.'

drawPoints :: S.Set Point -> String
drawPoints s = let
    adj = adjust s
    w = width s
    h = height s
    flat = [draw (x, y) adj | y <- [0..h-1], x <- [0..w-1]]
    in unlines . chunks w $ flat

moves :: S.Set Point -> (S.Set Point, Int)
moves = countMoves 0
    where
        countMoves :: Int -> S.Set Point -> (S.Set Point, Int)
        countMoves n a
            | width a < width (movePoints a) = (a, n)
            | otherwise = countMoves (n + 1) (movePoints a)

