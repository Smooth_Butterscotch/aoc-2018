main = do
    input <- getInput
    putStrLn $ showCell $ maxPower $ allSquares (read input) 300 300

getInput :: IO String
getInput = readFile "input.txt"

type Cell = (Int, Int)
type Serial = Int
type Power = Int
type PowerVal = (Power, Cell)

getPower :: Serial -> Cell -> Power
getPower s (x, y) = let
    rack = x + 10
    power = (rack * y + s) * rack
    dig = power `rem` 1000 `div` 100
    in dig - 5

getSquare :: Cell -> [Cell]
getSquare (x, y) = [(a, b) | a <- [x..x+2], b <- [y..y+2]]

squarePower :: Serial -> Cell -> Int
squarePower s = sum . map (getPower s) . getSquare

allSquares :: Serial -> Int -> Int -> [PowerVal]
allSquares s width height = let
    xs = [1..width-2]
    ys = [1..height-2]
    in [let c = (x, y) in (squarePower s c, c) | x <- xs, y <- ys]

maxPower :: [PowerVal] -> Cell
maxPower = snd . maximum

showCell :: Cell -> String
showCell (x, y) = show x ++ "," ++ show y

